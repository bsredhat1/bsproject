#!/bin/bash
ping -c2 192.168.0.1 > /dev/null
if [ $! = 0 ]; then 
echo " server pingable"
else
echo " server not reachable"
fi
